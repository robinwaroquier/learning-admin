<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;


class EditUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName', TextType::class, [
                'label'             => 'Prénom',
                'attr'              => [
                    "placeholder" => 'Prénom'
                ]
            ])
            ->add('lastName', TextType::class, [
                'label'             => 'Nom',
                'attr'              => [
                    "placeholder" => 'Nom'
                ]
            ])
            ->add('email', EmailType::class, [
                'label'             => 'Adresse E-mail',
                'attr'              => [
                    "placeholder" => 'Adresse E-Mail'
                ]
            ])
            ->add('imageFile', VichImageType::class, [
                'label'             => 'Votre avatar',
                'required'          => false,
                'download_label'    =>false,
                'image_uri'         =>false,
                'delete_label'      => false,
                'allow_delete'      => false,
                'attr'              => [
                    "placeholder" => 'Votre avatar',
                ]

            ]);
    }


    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class'    => User::class
        ]);
    }
}
